#!/usr/bin/env python3

import rospy
import numpy as np


from nav_msgs.msg import OccupancyGrid



def _reshape_msg2map():
    msg = rospy.wait_for_message('/map',OccupancyGrid)
    width = msg.info.width
    height = msg.info.height
    resolution = msg.info.resolution
    data = np.array(msg.data,dtype=np.int8)
    return data.reshape((height,width)) , resolution

def is_different(last_m_shape):

    newmap , resolution = _reshape_msg2map()
    if last_m_shape[0] != newmap.shape[0] or last_m_shape[1] != newmap.shape[1]:
        return newmap , resolution , True
    else:
        return newmap , resolution , False





