#!/usr/bin/env python3

import rospy
from std_srvs.srv import Trigger , TriggerResponse
from geometry_msgs.msg import PoseStamped



def trigger(req):
    print('did it')
    P = PoseStamped()
    P.header.stamp = rospy.Time()
    P.pose.position.x = 3.5
    return TriggerResponse(success=True , message = "Request has been made, executing the task")
    # return P
rospy.init_node('trigger_node' , anonymous=True)

new_service = rospy.Service('/path_seeking_request',Trigger,trigger)

rospy.spin()