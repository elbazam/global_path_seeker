#!/usr/bin/env python3

import rospy
import numpy as np
import time

from UISS import UISS_map
from matrix_check import is_different

from nav_msgs.msg import Odometry

from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Bool
from std_srvs.srv import Trigger , TriggerResponse

def publish_PoseStampe(loc,pub):

    P = PoseStamped()
    P.header.stamp = rospy.Time.now()
    
    P.pose.position.x = loc[0]
    P.pose.position.y = loc[1]
    for _ in range(5):
        pub.publish(P)

class exp:

    def __init__(self):
        '''Experiment class for trying UISS global planning method'''
        rospy.init_node('path_sender',anonymous=True)
        self.new_service = rospy.Service('/path_seeking_request',Trigger,self.generate_next_point)
        self.p_pub = rospy.Publisher('/next_goal' , PoseStamped , queue_size=1)
        self.path = []
        self._listener()
        
        self.map = np.zeros((20,20))
        self.resolution = 0.01
        self.UISS = UISS_map(self.map, start = [0,0],resolution = self.resolution)
        self.action = True
        self._nextpoints()
        
                     
    def generate_next_point(self,req):
        '''Service response'''
        if self.path:
            
            self.path.pop(0)
            if self.path:
                
                publish_PoseStampe(self.path[0], self.p_pub)
                return TriggerResponse(success=True , message = "Request has been made, sending next subgoal")
            else:
                print('Please input a new destination.')
                self.path = []
                self._nextpoints()
                if self.path:
                    self.path.pop(0)
                    publish_PoseStampe(self.path[0], self.p_pub)
                    return TriggerResponse(success=True , message = "New task has been recieved.")
                else:
                    
                    self.action = False
                    return TriggerResponse(success=False , message = "Finished the task, exit program.")
        else:
            print('Requiring a new goal')
            self.path = []
            self._nextpoints()
            if self.path:
                publish_PoseStampe(self.path[0], self.p_pub)
                return TriggerResponse(success=True , message = "New task has been recieved.")
            else:
                self.action = False 
                return TriggerResponse(success=False , message = "Finished the task, exit program.")

    def check_path(self):
        '''If path is not empty, pubish the path '''
        if self.path:
            self.p = self.path[0]
            publish_PoseStampe(self.p, self.p_pub)
            return True
        else:
            if self.action == False:
                return False
            else:
                return True
    
    def _odom_callback(self,msg):
        self.robot_location_msg = msg

    def _listener(self):
        '''Callback initialization '''
        rospy.wait_for_message('/mcl_pose',Odometry)
        rospy.Subscriber('/mcl_pose',Odometry,self._odom_callback)

    def _update_robot_location(self):
        '''Update the robot location'''
        self.robot_location = np.array([self.robot_location_msg.pose.pose.position.x,
                                    self.robot_location_msg.pose.pose.position.y])

    def _nextpoints(self):
        '''generate next goal and the path'''
        newmap , resolution , _ = is_different(self.map.shape)
        self._update_robot_location()
        self.UISS.update(self.robot_location,newmap,resolution)
        
        cond = input('To insert new goal press 1. To finish inserting press 0: ')
        cond = int(cond)
        if cond > 0:
            while(1):
                x,y = input('Input new x y location: ').split()
                x = float(x)
                y = float(y)

                point = self.UISS.world2maplocation(np.array([x,y]))
                
                if self.UISS.set_goal(point):
                    self.action = True
                    t = time.time()
                    print('Path planning begins now.')
                    self.UISS.UISSPathPlanning()
                    if not self.UISS.Real_path:
                        print('Unreachable, insert new goal.')
                        continue
                    self.path = self.UISS.Real_path
                    print('Planning took ' , round(time.time() - t,3) , 'seconds')
                    break
                
        else:
            self.action = False
            
            

e = exp()

while not rospy.is_shutdown():
    if not e.check_path(): break
    

print('Goodbye until next time.')



