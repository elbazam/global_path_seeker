#!/usr/bin/env python3
import rospy
from std_srvs.srv import Trigger, TriggerResponse



rospy.init_node('call_service_node',anonymous = True)

rospy.wait_for_service('/path_seeking_request')
add_two_ints = rospy.ServiceProxy('/path_seeking_request',Trigger)

print(add_two_ints())