#!/usr/bin/env python3

import rospy
import numpy as np


from nav_msgs.msg import OccupancyGrid
from visualization_msgs.msg import MarkerArray,Marker


path_pub = rospy.Publisher('/path/remaining_path' , MarkerArray, queue_size = 1)


def dist(p,q):
    return ((p[0]-q[0])**2 + (p[1]-q[1])**2)**0.5

def _reshape_msg2map():
    msg = rospy.wait_for_message('/map',OccupancyGrid)
    width = msg.info.width
    height = msg.info.height
    resolution = msg.info.resolution
    data = np.array(msg.data,dtype=np.int8)
    return data.reshape((height,width)) , resolution

def is_different(last_m_shape):

    newmap , resolution = _reshape_msg2map()
    if last_m_shape[0] != newmap.shape[0] or last_m_shape[1] != newmap.shape[1]:
        return newmap , resolution , True
    else:
        return newmap , resolution , False


def _angle_check(p,q,theta):

    angle = np.absolute(np.arctan2(p[1]-q[1],p[0]-q[0])-theta)
    if angle > np.pi/4:
        return False
    return True


def new_list(p_list , q ,theta = 0, threshold = 1.0 , msg = False):
    '''pop from p_list until p_list next point is close to q'''
    if not p_list: return []
    
    p = p_list[-1]
    while p_list[0] != p:
        if dist(p_list[0],q) > threshold: break
        if _angle_check(p_list[0],p_list[1],theta): break
        if msg:
            print('Poping the close point: ' + str([round(p_list[0][0],2),round(p_list[0][1],2)]))
        p_list.pop(0)
        if msg and p_list:
            print('Next subgoal is: ' + str([round(p_list[0][0],2),round(p_list[0][1],2)]))
        
    
    
    return p_list
    
def quaternion_to_euler(x, y, z, w):
    '''Input quanterion angle.
    Return euler angles [Yaw,Pitch,Roll]'''
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll = np.arctan2(t0, t1)
    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch = np.arcsin(t2)
    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw = np.arctan2(t3, t4)
    return [yaw, pitch, roll]    






def publish_remaining_path_in_rviz(points):

    A = MarkerArray()
    m = Marker()
    m.action = 3
    m.header.frame_id = 'world'
    A.markers.append(m)
    for _ in range(3):
        path_pub.publish(A)
    A = MarkerArray()
    for i,p in enumerate(points):
        m = Marker()
        m.header.frame_id = 'world'
        m.action = 0
        m.type = 2 # Sphere
        m.id = i
        m.pose.position.x = p[0]
        m.pose.position.y = p[1]
        m.pose.position.z = 0.5

        m.pose.orientation.x = 0
        m.pose.orientation.y = 0
        m.pose.orientation.z = 0
        m.pose.orientation.w = 1

        m.scale.x = 0.2
        m.scale.y = 0.2
        m.scale.z = 0.2
        m.color.r = 0

        if i == 0: 
            m.color.g = 255
            m.color.b = 0
        else:
            m.color.g = 0
            m.color.b = 255
        m.color.a = 1
        A.markers.append(m)
    for _ in range(3):
        path_pub.publish(A)
