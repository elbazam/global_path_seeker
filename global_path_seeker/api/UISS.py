#!/usr/bin/env python3


import numpy as np
import cv2
from heapq import heappush, heappop



def reconstract_map(m):
    '''Reconstract the map to fit the UISS algorithm'''
    m[m == -1] = 100
    m = 250 - 125*(np.array(m/50,dtype=np.uint8))
    return m

def distance(p, q):
    """Helper function to compute distance between two points."""
    return np.sqrt((p[0]-q[0])**2 + (p[1]-q[1])**2)



class UISS_map:
    ''' 
        Create a search graph using UISS algorithm taken from  "A Review of Global Path Planning Methods for Occupancy
        Grid Maps Regardless of Obstacle Density" by: E. G. Tsardoulias · A. Iliakopoulou ·
        A. Kargakos · L. Petrou.
        The A* algorithm was taken from youtube "Claus Brenner" channel from his SLAM Lectures.
    '''
    def __init__(self, m , start = [0,0] ,resolution = 0.10 , s = 5 , MDW = 3):
        '''
            Insert occupancy grid map as a matrix, resolution of the map, size of step (true length / resolution)
            and MDW as the minimal distance from an obsticle.
        '''
        self.update(start , m , resolution)
        
        
        
        self.MDW = MDW
        self.s = s
        self.movement = ((s,0), (0,s), (-s,0), (0,-s),
                          (s,s), (-s,s), (-s,-s), (s,-s))
        
        
        
    def update(self,start,m,resolution):
        
        self.resolution = resolution
        self.map =  reconstract_map(m)
        self.distMap = cv2.distanceTransform(self.map, cv2.DIST_L1,5)
        self.occupied_map = np.array(self.map,dtype = np.bool)
        self.start = self.world2maplocation(start)
        self.UISS_HIST = -np.ones(self.map.shape)
        self._find_xy_min()
        self.G = {}
        
        
        self.counter = 0
        
        self.cond1 = True
        self.cond2 = True
        self.path = []
        self.Real_path = []

    def _find_xy_min(self):
        '''Part of the algorithm, find where the map begines '''

        self.xmin = 0
        self.ymin = 0

        for i in range(self.map.shape[0]):
            
            if np.sum(self.occupied_map[i,:]) < self.map.shape[0]:
                self.xmin = i
                break
            
        for j in range(self.map.shape[1]):
            if np.sum(self.occupied_map[:,j]) < self.map.shape[1]:
                self.ymin = j
                break
        
    def set_goal(self,goal):
        '''
            Set a new goal
            True - can be reached
            False - cannot be reached
        '''

        x = int(goal[0])
        y = int(goal[1])
        
        if x < 0 or x >= self.map.shape[0]:
            print('Ureachable, insert new goal.')
            return False
        if y < 0 or y >= self.map.shape[1]:
            print('Ureachable, insert new goal.')
            return False
        if self.distMap[x,y] < self.MDW:
            print('Ureachable, insert new goal.')
            return False
        
        self.goal = (x,y)
        print('New goal is valid.')
        return True
    
    
    def remove_connections(self):
        '''After a new goal has been requested, eliminate the previous connections to the graph'''
        if self.cond1:
            for a in self.G[tuple(self.start)]:
                p = np.array(self.start) + np.array(a)
                a = np.array(a)
                self.G[tuple(p)].remove(tuple(-a))
            try:
                del self.G[tuple(self.start)]
            except: pass
        if self.cond2:
            try:
                del self.G[tuple(self.goal)]
            except: pass
        self.start = self.goal
        self.cond1 = True
        self.cond2 = True
            
            
    def UISSPathPlanning(self):
        '''The path planning'''
        
        self._CreateUISSGraph()
        
        K = self.G.keys()
        
        if tuple(self.start) not in K: self.G[tuple(self.start)] = []
        else: self.cond1 = False
        if tuple(self.goal) not in K: self.G[tuple(self.goal)] = []
        else: self.cond2 = False
        if self.cond1 or self.cond2:
            
            for n in self.G.keys():
                
                ps = np.absolute(np.array(n)-np.array(self.start))
                pg = np.absolute(np.array(n)-np.array(self.goal))
                if self.cond1:
                    
                    if ps[0] < self.s and ps[1] < self.s:
                        if ps[0]**2 + ps[1]**2 != 0:
                            
                            p = np.array(n)-np.array(self.start)
                            
                            self.G[tuple(self.start)].append(tuple(p))
                            self.G[tuple(n)].append(tuple(-p))
                if self.cond2:
                    
                    if pg[0] < self.s and pg[1] < self.s:
                        if pg[0]**2 + pg[1]**2 != 0:
                            
                            p = np.array(n)-np.array(self.goal)
                            self.G[tuple(self.goal)].append(tuple(p))
                            self.G[tuple(n)].append(tuple(-p))
        
        self.path,_ = self._AstarAlgorithm()
        self.Real_path = self._reconstract_path()
                    
                    
    def _CreateUISSGraph(self):
        
        '''Creating the graph or update if the graph already has been made.'''
        if self.G:
            self.remove_connections()
        
        xt0 = int(self.map.shape[0]/2 - (((self.map.shape[0]/2-self.xmin))/self.s)*self.s)
        yt = int(self.map.shape[1]/2 - (((self.map.shape[1]/2-self.ymin))/self.s)*self.s)
        
        
        while yt + self.s < self.map.shape[1]:
            yt += self.s
            xt = xt0
            while xt + self.s < self.map.shape[0]:
                xt += self.s
                nk = tuple([xt,yt])
                cond = self.distMap[nk[0],nk[1]] > self.MDW
                if self.UISS_HIST[nk[0],nk[1]] != -1:
                    if not cond:
                        self.G.pop(nk)
                else:
                    if not cond:
                        continue
                    l = []
                    for m in self.movement:
                        Np = np.array(nk) + m
                        if Np[0] > self.map.shape[0] or Np[1] > self.map.shape[1]:
                            continue
                        
                        if self.distMap[Np[0],Np[1]] > self.MDW:
                            l.append(m)
                    if l:
                        self.G[nk] = l
                        self.counter += 1
                        
    
    def _AstarAlgorithm(self):
        
        '''Graph search algorithm'''
        
        goal = [int(_) for _ in self.goal]
        start = [int(_) for _ in self.start]
        dist = distance(start , goal) + 0.001
        front = [ (dist , 0.001 , start, None) ]
    
        # In the beginning, no cell has been visited.
        extents = self.map.shape
        visited = np.zeros(extents, dtype=np.float32)
    
        # Also, we use a dictionary to remember where we came from.
        came_from = {}
    
        # While there are elements to investigate in our front.
        while front:
            # Get smallest item and remove from front.
            element = heappop(front)
            
            _, cost, pos, previous = element[0],element[1],\
                                                element[2],element[3]
    
            # Now it has been visited. Mark with cost.
            pos = [int(_) for _ in pos]
            
            
            # Now it is visited. Mark with cost.
            if visited[tuple(pos)] > 0: continue
            else: visited[tuple(pos)] = cost
    
            
            came_from[tuple(pos)] = previous
            
            # Also remember that we came from previous when we marked pos.
    
            # Check if the goal has been reached.
            if pos == goal:
                print('A* found a path!')
                break  # Finished!
    
            # Check all neighbors.
            for dx, dy in self.G[tuple(pos)]:
                deltacost = distance([0,0],[dx,dy])
                loc = tuple(map(sum, zip(pos, tuple((dx,dy)))))
                new_x , new_y = loc[0] , loc[1]
                if new_x < 0 or new_x >= extents[0]:
                    continue
                if new_y < 0 or new_y >= extents[1]:
                    continue
                # Add to front if: not visited before and no obstacle.
                new_pos = (int(new_x), int(new_y))
                if visited[tuple(new_pos)] == 0:
                    new_cost = cost + deltacost
                    new_total_cost = distance(new_pos,goal) + new_cost
                    heappush(front,(new_total_cost,new_cost,new_pos,pos))
    
        
        path = []
        if pos == goal:  # If we reached the goal, unwind backwards.
            while pos:
                path.append(pos)
                pos = came_from[tuple(pos)]
            path.reverse()  # Reverse so that path is from start to goal.
    
        return (path, visited)
     

    def _reconstract_path(self):
        '''Generate the path as real world coordinates'''
        
        origin = np.array(np.array(self.map.shape)/2,dtype = np.int16)
        rec_path = []
        
        for p in self.path:
            x = (p[1] - origin[1])*self.resolution
            y = (p[0] - origin[0])*self.resolution
            rec_path.append(tuple([x,y]))
        return rec_path
    
    
    def world2maplocation(self , point):
        '''Transfer the request from real world to the map world '''
        new_point = np.array(point) / self.resolution
        
        x = self.map.shape[0]/2
        y = self.map.shape[1]/2
        origin = np.array([x,y],dtype = np.int16)
        new_point = tuple([int(new_point[1] + origin[1]),int(new_point[0] + origin[0])])
        
        return new_point

        