import setuptools

setuptools.setup(
    name="global_path_seeker",
    version="0.1",
    author="Amit Elbaz",
    author_email="aoosha@gmail.com",
    packages=['global_path_seeker'],
    python_requires='>=3.5',
)
