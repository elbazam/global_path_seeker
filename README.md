## Description

Global planner using UISS algorithm. Based on a published papar: "A Review of Global Path Planning Methods for Occupancy Grid Maps Regardless of Obstacle Density"

## Video example:


[![Video](https://img.youtube.com/vi/d4G8Tfo-Srk/0.jpg)](https://youtu.be/d4G8Tfo-Srk)

## Dependencies

- python version > 3.5.  
- numpy   
- opencv  
- heapq
- hector_mapping http://wiki.ros.org/hector_mapping
- robot_description https://github.com/TalFeiner/robot_description  

## Setup

Clone and install repository:
```sh
$ cd ~/your_catkin_worspace/src
$ git clone https://elbazam@bitbucket.org/elbazam/global_path_seeker.git
$ pip3 install -e ./global_path_seeker
```  

For robot_description package:

```sh
$ cd ~/your_catkin_worspace/src
$ git clone https://github.com/TalFeiner/robot_description.git
$ cd ..
$ catkin_make
```

In ~/robot_description/blattoidea/urdf/blattoidea_gazebo.urdf.xarco, change the base_laser configuration to the following:  
- xyz="0.17 0.0 0.085" rpy="0 0 0"



## Execution

* To execute the package:
```sh
$ rosrun global_path_seeker Path_planner.py
```
* Publish '/move_base_simple/goal' topic to determine your destination (e.g. use rviz as shown in the video above)


## Notes
* This package is sycronized with potential_navigation package ,https://bitbucket.org/elbazam/potential_navigation/src/master/ , install it for optimal performance.

## Topics flowchart:

![topicflowchart](media/blocks_global.png)


### Who do I talk to? ###

* Repo owner or admin
